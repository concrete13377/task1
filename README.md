# task1

### build docker image (if you want, but it will take time due to dlib install): 
```
sudo docker concrete13377/task1 build .  
```

### for all
```
export QT_X11_NO_MITSHM=1  
xhost +si:localuser:root  
```

### for webcam
```
sudo docker run --device=/dev/video0:/dev/video0 -v /home/user/.Xauthority:/home/user/.Xauthority -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY concrete13377/task1  
```

### for video 
```
sudo docker run -v local/path/for/video.mp4:/video.mp4 --device=/dev/video0:/dev/video0 -v /home/user/.Xauthority:/home/user/.Xauthority -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY concrete13377/task1 --video video.mp4  
```

the code has 4 cl arguments:  
-p, --shape-predictor, path to facial landmark predictor,  
-v, --video, path to input video file,  
-t, --ear-thresh, eye ar thershold,  
-f, --frames-thresh, min frames that eyes should be closed to count as blink  