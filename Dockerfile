FROM python:3
ADD task.py /
ADD predictor.dat /
#RUN pip3 install
RUN apt-get update -y  && apt-get install build-essential cmake libopenblas-dev liblapack-dev libx11-dev libgtk-3-dev python3-dev python3-pip -y --no-install-recommends  && rm -rf /var/lib/apt/lists/*
#RUN apt-get install  -y
#RUN apt-get install  -y
#RUN apt-get install python3-dev python3-pip -y
RUN pip3 install scipy opencv-python dlib imutils argparse
#RUN pip3 install

#RUN pip3 install
#RUN pip3 install

ENTRYPOINT ["python3", "./task.py"]

